#!/usr/bin/env python

import signal
import sys

from geometry_msgs.msg import Twist

# subscribers/publishers namespace
ns = {
    'sub' : {
        'control'        : '/lurch_control',
        'sonar'          : '/RosAria/sonar',
        'cmd_vel_avoid'  : '/cmd_vel_avoid',
        'odometry'       : '/tracker/markers_array_smoothed',
        'move_base'      : '/move_base',
        'move_base_goal' : '/move_base_simple/goal' },
    'pub'                : {
        'control'        : '/lurch_control',
        'cmd_vel'        : '/RosAria/cmd_vel',
        'cmd_vel_avoid'  : '/cmd_vel_avoid' }
}

# common superclass
class Base(object):
    def __init__(self):
        # SIGINT handler
        signal.signal(signal.SIGINT, self.signal_handler)

        # data holders
        initSon      = type('Dummy', (object,), {"x":0.0, "y":0.0, "z":0.0})
        self.sonData = [initSon for i in range(16)]
        self.conMsg  = {"wandering":0, "collision_avoidance":0, "tracking":0}

    def signal_handler(self, signal, frame):
        self.stop()
        sys.exit(0)

    def stop(self):
        self.velPub.publish(Twist())

    # fetch p3dx data (16 sonars)
    def processSonar(self, sonMsg):
        if len(sonMsg.points) == 16:
            for i in range (16):
                self.sonData[i] = sonMsg.points[i]
