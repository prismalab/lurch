#!/usr/bin/env python

import random
import rospy
import geometry_msgs.msg
import sensor_msgs.msg

from base import Base
from lurch.msg import Control

class Wandering(Base):
    def __init__(self, nameSpace):
        super(Wandering, self).__init__()

        rospy.init_node("wandering")

        # using a custom name space (dict)
        self.ns = nameSpace

        # wandering flag
        self.do_wandering = 1

        # subscribers
        self.controlSub = rospy.Subscriber(self.ns["sub"]["control"], Control, self.processControl)
        self.sonSub = rospy.Subscriber(self.ns["sub"]["sonar"], sensor_msgs.msg.PointCloud, self.processSonar)

        # publishers
        self.velPub = rospy.Publisher(self.ns["pub"]["cmd_vel_avoid"], geometry_msgs.msg.Twist, queue_size=10)

    def processControl(self, data):
        self.do_wandering = data.wandering
        print(data.wandering)

    def wander(self, refresh_r):
        rate = rospy.Rate(refresh_r)
        vel = geometry_msgs.msg.Twist()
        while not rospy.is_shutdown():
            if self.do_wandering:
                vel.linear.x = random.choice([0.2, 0.0])
                if vel.linear.x == 0:
                    rospy.loginfo("random rotation")
                    # rotate torwards less obstacled side; in doing so, put more weight on lateral sonars
                    if ((self.sonData[7].y * -3 + self.sonData[6].x * 1 + self.sonData[5].x * 0.5 ) >
                        (self.sonData[0].y * 3 + self.sonData[1].x * 1 + self.sonData[2].x * 0.5)):
                        vel.linear.x = 0.2
                        vel.angular.z = -0.3
                        self.velPub.publish(vel)
                    else:
                        vel.linear.x = 0.2
                        vel.angular.z = 0.3
                        self.velPub.publish(vel)
                else:
                    rospy.loginfo("going straight")
                    vel.angular.z = 0.0
                    self.velPub.publish(vel)
            rate.sleep()

if (__name__ == "__main__"):
    ns = {}
    ns["sub"] = {}
    ns["pub"] = {}
    ns["sub"]["control"] = "/lurch_control"
    ns["sub"]["sonar"] = "/RosAria/sonar"
    ns["pub"]["cmd_vel_avoid"] = "/cmd_vel_avoid"

    w = Wandering(ns)
    w.wander(10)
