# lurch
## Low cost bUtler Robot for elderly Care Help

----------------------------------------------

### Authors

Luca Capezzuto, Salvatore Giugliano, Marco Grazioso, Marco Valentino

### License

[GNU-GPL2](http://www.gnu.org/licenses/gpl-2.0.html)

### Source

https://bitbucket.org/lurchdev/lurch

### Description

A ROS Indigo package with the objective of providing functionalities for setting up an elderly assistance robot.

We use a [P3-DX](http://www.mobilerobots.com/ResearchRobots/PioneerP3DX.aspx) and a Kinect v1.

### Features

1. Wandering and sonar-based collision avoidance
2. Environment map generation
3. Goal-oriented navigation
4. Human identification and tracking
5. Voice navigation

---

# Commands

## Initialization

**Note:** not needed for tracking.

Launch OpenNI and RosAria:

```shell
$ roscore
$ roslaunch lurch init.launch
```

then launch transformations:

```shell
$ roslaunch lurch transformations.launch
```

## Mapping

Start mapping:

```shell
$ roscd lurch/bin
$ ./mapping_start.sh
```

move the robot to acquire the data, either with [rosaria_client](https://github.com/pengtang/rosaria_client) teleop:

```shell
$ rosrun rosaria_client teleop
```

or with our wandering and collision avoidance module:

```shell
$ roslaunch lurch wca.launch
```

if you wish to check the creation process, run RViz:

```shell
$ roscd lurch/configs/rviz
$ rosrun rviz rviz -d conf.rviz
```

when you're satisfied with the map, save it and stop the process:

```shell
$ roscd lurch/bin
$ ./mapping_stop.sh /path/where/to/save/the/map
```

**note:** In order to do a good mapping, assure that `laserscan` is perpendicular to Kinect `pointcloud` (you can check it in RViz). 
A good Kinect inclination for mapping/navigation is `-5`. See the following *Other* section to know how to set it.

## Navigation

Load the map:

```shell
$ rosrun map_server map_server /path/to/map.yaml
```

then launch AMCL and `move_base`:

```shell
$ roslaunch lurch navigation.launch
```

**note**: navigation strictly depends from the initial pose, so be sure that it's correct before giving a goal point.

You can also use `goto_location`, which will subscribe to `/move_base_simple/goal` to fecth RViz goals:

```shell
$ rosrun lurch goto_location.py
```

this allows RViz to be run in remote, lightening the computational load for the machine directly connected to the robot.


## Tracking

Start RosAria:

```shell
rosrun rosaria RosAria
```

Launch detection and tracking (visual):

```shell
roslaunch tracking detection_and_tracking.launch
```

then launch our tracking (motion):

```shell
$ roslaunch lurch tracking.launch
```

**Note:** tracking algorithm uses the ground plan as a reference, so the kinect should have a degree of inclination. Lowest inclination is `-15`. See the following *Other* section to know how to set it.

## Voice teleoperation

Set `mic_name` parameter in `lurch/launch/voice_nav.launch` to your microphone name. To know how, give the command:

```shell
$ pacmd list-sources
```

and look among input device identifiers. Then, launch [pocketsphinx](http://wiki.ros.org/pocketsphinx) recognizer:

```shell
$ roslaunch lurch voice_nav.launch
```

the default vocabulary is English, but you can create your own reading the section "Creating a Vocabulary" in [this guide](http://www.pirobot.org/blog/0022/).

Finally, launch our voice teleoperation node:

```shell
$ rosrun lurch voice_nav.py
```

Beside the microphone, you can also use the text-to-speech node in [sound_play](http://wiki.ros.org/sound_play):

```shell
$ roslaunch sound_play soundplay_node.launch
$ rosrun sound_play say.py "your command"
```

Following are the keyword commands for English:

| **command**  | **mapped strings** |
| -------------|------------------------------
| stop         | stop, halt, abort, kill, panic, off, freeze, shut down, turn off, help, help me|
| slower       | slow down, slower|
| faster       | speed up, faster|
| forward      | forward, ahead, straight|
| backward     | back, backward, back up|
| rotate left  | rotate left, left|
| rotate right | rotate right, right|
| turn left    | turn left, left|
| turn right   | turn right, right|
| quarter      | quarter speed|
| half         | half speed|
| full         | full speed|
| pause        | pause speech|
| continue     | continue speech|


## Laptop battery monitor

Check

```shell
$ ls /sys/class/power_supply
```

in order to know your `BAT*` identifier, then set `battery` parameter in `lurch/launch/laptop_battery_monitor.launch` accordingly, and launch it:

```shell
$ roslaunch lurch laptop_battery_monitor.launch
```

**Note:** requires [laptop_battery_monitor](http://wiki.ros.org/laptop_battery_monitor).


### Other



Kill all ROS processes:

```shell
$ pkill -f ros; pkill -f nodelet; pkill -f XnSensor
```

Set Kinect inclination to X

```shell
$ rosrun kinect_aux kinect_aux_node
$ rostopic pub /tilt_angle std_msgs/Float64 -- X
```
    
Kinect calibration (sources: [1](http://wiki.ros.org/openni_launch/Tutorials/IntrinsicCalibration) [2](http://wiki.ros.org/camera_calibration/Tutorials/MonocularCalibration)), to be done only once:

```shell
$ rosrun camera_calibration cameracalibrator.py image:=/camera/rgb/image_raw camera:=/camera/rgb --size 8x6 --square 0.0484
$ rosrun camera_calibration cameracalibrator.py image:=/camera/ir/image_raw camera:=/camera/ir --size 8x6 --square 0.0484
```