#! /bin/bash

# stops creation of map
# takes map path/name as argument, for instance:
# $ roscd lurch
# $ bin/mapping_stop.sh /path/to/map

# note: the path must be absolute

if [ -z $1 ]; then
    echo "Usage: ./mapping_stop.sh absolute/path/to/map"
    exit 1
else
    rosrun map_server map_saver -f $1 &&
    rosnode kill slam_gmapping &&
    rosnode kill wandering &&
    rosnode kill collision_avoidance &&
    rostopic pub -1 /RosAria/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
    exit 0
fi
