#! /bin/bash

# starts creation of a map

# PRECOND: roslaunch lurch transformations.launch

# to check the map in generation:
# $ rosrun rviz rviz -d /path/to/lurch/configs/conf.rviz

rosparam set /slam_gmapping/angularUpdate 0.1 &&
rosparam set /slam_gmapping/linearUpdate 0.1 &&
rosparam set /slam_gmapping/lskip 10 &&
rosparam set /slam_gmapping/xmax 10 &&
rosparam set /slam_gmapping/xmin -10 &&
rosparam set /slam_gmapping/ymax 10 &&
rosparam set /slam_gmapping/ymin -10 &&
rosrun gmapping slam_gmapping scan:=/scan

# you can launch a motion node now, like teleop or 'roslaunch lurch wca.launch'
